package id.jakaria.loket;

import org.omg.CORBA.CustomMarshal;

import java.util.List;

public interface CustomerService {
    Customer create(Customer customer);
    List<Customer> findAll();
    Customer findById(Long id);
}
