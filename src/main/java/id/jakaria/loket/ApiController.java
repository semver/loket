package id.jakaria.loket;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ApiController {

    private CustomerService customerService;

    public ApiController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping(value =  "")
    public ResponseEntity<Customer> getCustomer() {
        Customer createdCustomer = new Customer("jaka", "zakaria@kofera.com");

        Customer custmerWithId = this.customerService.create(createdCustomer);

        return new ResponseEntity<>(custmerWithId, HttpStatus.CREATED);
    }

    @GetMapping(value = "all")
    public ResponseEntity<List<Customer>> allCustomer() {
        List<Customer> customerList = this.customerService.findAll();
        return new ResponseEntity<>(customerList, HttpStatus.OK);
    }

    @GetMapping(value = "get/{id}")
    public ResponseEntity<Customer> findById(@PathVariable Long id) {
        Customer customer = this.customerService.findById(id);
        return new ResponseEntity<>(customer, HttpStatus.CREATED);
    }

}
