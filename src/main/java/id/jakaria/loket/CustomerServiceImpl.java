package id.jakaria.loket;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {
    private CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer create(Customer customer) {
        return this.customerRepository.save(customer);
    }

    @Override
    public List<Customer> findAll() {
        Iterable<Customer> customers =  this.customerRepository.findAll();
        List<Customer> customerList = new ArrayList<>();

        customers.forEach(customerList::add);

        return customerList;
    }

    @Override
    public Customer findById(Long id) {
        return customerRepository.findById(id).orElse(new Customer());

    }
}
